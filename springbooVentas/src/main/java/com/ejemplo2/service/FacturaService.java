package com.ejemplo2.service;

import com.ejemplo2.model.FacturaModel;


public interface FacturaService {
	public void save(FacturaModel facturaModel);
	public void update(FacturaModel facturaModel);
	public void delete(int id);
	public FacturaModel findById(int id);
}
