package com.ejemplo2.dao;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.time.LocalDateTime;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

import com.ejemplo2.model.DetalleFacturaModel;
import com.ejemplo2.model.FacturaModel;

@Repository
public class FacturaDaoImpl implements FacturaDao{
	
	@Autowired
	private JdbcTemplate jdbcTemplate;
	
	@Override
	public void save(FacturaModel facturaModel) {
		jdbcTemplate.update("insert into factura(idcliente,numfacura,fechacreacion)values(?,?,?)",
				facturaModel.getIdcliente(),facturaModel.getNumfacura(),facturaModel.getFechacreacion());
		
	}
	
	@Override
	public void update(FacturaModel facturaModel) {
		jdbcTemplate.update("update factura set idcliente=?,numfacura=?,fechacreacion=? where idfactura=?",
				facturaModel.getIdcliente(),facturaModel.getNumfacura(),facturaModel.getFechacreacion());
	}

	
	@Override
	public FacturaModel findById(int id) {
		
		return jdbcTemplate.queryForObject("select * from factura where idfactura=?", new FacturaModelRowMapper(), id); 	        
	}

	@Override
	public void delete(int id) {
		 jdbcTemplate.update("delete from factura where idfactura=?", id); 	
	}

}
