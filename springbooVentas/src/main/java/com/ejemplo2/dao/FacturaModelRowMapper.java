package com.ejemplo2.dao;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.time.LocalDateTime;

import org.springframework.jdbc.core.RowMapper;


import com.ejemplo2.model.FacturaModel;

public class FacturaModelRowMapper implements RowMapper<FacturaModel> {

	@Override
	public FacturaModel mapRow(ResultSet rs, int rowNum) throws SQLException {
		
		LocalDateTime fechacreacion = LocalDateTime.now();
		
		FacturaModel facturaModel=new FacturaModel();
	 	facturaModel.setIdfactura(rs.getInt("idfactura"));
	 	facturaModel.setIdcliente(rs.getInt("idcliente"));
	 	facturaModel.setNumfacura(rs.getString("numfacura"));
	 	facturaModel.setFechacreacion(fechacreacion);
	 	
	 	return facturaModel;
	}

}
