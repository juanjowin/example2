package com.ejemplo2.dao;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

import com.ejemplo2.model.ClienteModel;

@Repository
public class ClienteDaoImpl implements ClienteDao{
	
	@Autowired
	private JdbcTemplate jdbcTemplate;
	

	@Override
	public int save(ClienteModel clienteModel) {
		return jdbcTemplate.update("insert into cliente(nombre,apellido,telefono,email)values(?,?,?,?)",clienteModel.getNombre(),
				 clienteModel.getApellido(),clienteModel.getTelefono(),clienteModel.getEmail());
		
	}
	

}
