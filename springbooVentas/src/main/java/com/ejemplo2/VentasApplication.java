package com.ejemplo2;

import java.awt.List;
import java.sql.Date;
import java.time.LocalDateTime;
import java.util.ArrayList;

import org.hibernate.validator.constraints.Length;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;


import com.ejemplo2.model.ClienteModel;
import com.ejemplo2.model.DetalleFacturaModel;
import com.ejemplo2.model.FacturaModel;
import com.ejemplo2.model.ProductoModel;
import com.ejemplo2.service.ClienteServiceImpl;
import com.ejemplo2.service.DetalleFacturaService;
import com.ejemplo2.service.FacturaService;
import com.ejemplo2.service.ProductoService;



@SpringBootApplication
public class VentasApplication implements CommandLineRunner{
	
	 
	public static void main(String[] args) {
		SpringApplication.run(VentasApplication.class, args);
		
	}

	@Override
	public void run(String... args) throws Exception {
		
	}
							
}	
	


